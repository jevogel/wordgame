# Aa to Zyzzyvas: A Wordgame #
To try the game out in its native form, visit my website
[here](http://jimvog.com/wordgame). This is an HTML page that uses WebSockets to
communicate with the Python game script which is hosted with
[websocketd](http://websocketd.com).

## How to Play ##
"I'm thinking of a word between 'aa' and 'zyzzyvas'. Can you guess what I'm
thinking? Each time you guess, I'll tell you if you have guessed correctly, or
if not, I'll tell you the new word range. Try to get it in the fewest guesses.
If you're having a hard time, don't fret: I'll let you peek at the dictionary if
there are ten or fewer possible words left. Good luck!"

## About the Game ##
Aa to Zyzzyvas is based on the game Abacus to Zuni. Abacus to Zuni was an IRC
chat game that my friends and I had a good time playing on Ethan Zonca's IRC
server in high school. I was recently thinking about this game and decided to
run a search for it, only to come up empty-handed. So I decided to create my own
version of it. I later found that there are other references to this game as
simply 'IRC word game'. If you know anything about Abacus to Zuni, feel free to
drop me a line. Enjoy!

## About the Dictionaries ##
Both dictionaries can be found [here](https://github.com/dolph/dictionary). The dictionary
used for Lexicographer is the Enable1 dictionary, comprised of a staggering
172,823 words, which is based on the Official Scrabble Player's Dictionary but
with the addition of words longer than eight letters. Neophyte uses the Popular
dictionary, comprised of a much more reasonable 25,332 words, which represets
the common subset of words found in both Enable1 and Wiktionary's word frequency
lists.
