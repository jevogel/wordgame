#!/usr/bin/env python3

# If running on server, set to True.
# If running in command line, set to False.
WEB = True

from sys import stdin, stdout
from os import path
from operator import itemgetter
from time import strftime, sleep
import pickle
import random
if WEB:
    import fcntl
    import errno

if WEB:
    PATH = '/var/www/wordgame/'
else:
    PATH = ''

DICT_NAMES = ['Neophyte', 'Lexicographer']

# Screen Width in chars
SW = 42

def prompt():
    print('> ')
    stdout.flush()

def print_line(string=''):
    print(string)
    if WEB:
        stdout.flush()

def get_input(string=''):
    if WEB:
        if string:
            print_line(string)
        prompt()
        line = stdin.readline().strip()
        print_line(line)
    else:
        line = input(string + '> ')
    return line.lower()

def get_word(guesses, low_word, high_word):
    return get_input("({}) {} to {}.".format(guesses, low_word.title(), high_word))

def game_intro(title=False):
    if title:
        print_line(' Welcome to Aa to Zyzzyvas! '.center(SW, '='))
        print_line(' A Word Game by Jimmy Vogel '.center(SW, '='))
        print_line()
        print_line('To start a game, say "start".')
    else:
        print_line('To play again, say "start".')
    print_line('For how to play, say "help".')
    print_line('For more about the game, say "about".')
    print_line('To see the Leaderboards, say "score".')
    if WEB:
        print_line('To clear the screen, say "clear".')
    else:
        print_line('To leave, say "quit".')
    print_line()

def parse_cmd(input_cmd):
    word_lists = [[],[]]
    if 'start' in input_cmd:
        print_line()
        print_line('First, choose a difficulty:')
        print_line('0: {} (include common words)'.format(DICT_NAMES[0]))
        print_line('1: {} (include uncommon words)'.format(DICT_NAMES[1]))
        difficulty = get_input('(Say "0" or "1")')
        while difficulty != '0' and difficulty != '1':
            difficulty = get_input(
                'Say "0" and I will select a relatively common word. '
                'Say "1" to include less common words.')

        difficulty = int(difficulty)

        print_line()
        print_line('Okay, reading my {} dictionary...'.format(DICT_NAMES[difficulty]))

        word_lists[0] = [line.rstrip('\n') for line in open(PATH+'popular.txt')]
        word_lists[1] = [line.rstrip('\n') for line in open(PATH+'enable1.txt')]

        game(word_lists, difficulty)
    elif 'help' in input_cmd:
        print_line("\n"
"I'm thinking of a word between 'aa' and 'zyzzyvas'. "
"Can you guess what I'm thinking? Each time you guess, "
"I'll tell you if you have guessed correctly, or if not, "
"I'll tell you the new word range. Try to get it in the "
"fewest guesses. If you're having a hard time, don't fret: "
"I'll let you peek at the dictionary if there are ten or "
"fewer possible words left. Good luck!\n")
    elif 'about' in input_cmd:
        print_line("\n"
"ABOUT THE GAME:\n"
"Aa to Zyzzyvas is based on the game Abacus to Zuni. "
"Abacus to Zuni was an IRC chat game that my friends "
"and I had a good time playing on Ethan Zonca's IRC "
"server in high school. I was recently thinking about "
"this game and decided to run a search for it, only "
"to come up empty-handed. So I decided to create my "
"own version of it. I later found that there are other "
"references to this game as simply 'IRC word game'. "
"If you know anything about Abacus to Zuni, feel free "
"to drop me a line at jimmy@jimvog.com. Enjoy!\n\n"
"ABOUT THE DICTIONARIES:\n"
"Both dictionaries can be found at dolph/dictionary "
"on GitHub. The dictionary used for Lexicographer is "
"the Enable1 dictionary, comprised of a staggering 172,823 words, "
"which is based on the Official Scrabble Player's Dictionary "
"but with the addition of words longer than eight letters. "
"Neophyte uses the Popular dictionary, comprised of a "
"much more reasonable 25,332 words, which represets the "
"common subset of words found in both Enable1 and Wiktionary's "
"word frequency lists.\n")

    elif 'score' in input_cmd:
        scoring(add=False)
    elif WEB and 'clear' in input_cmd:
        print_line('Clearing...')
    elif not WEB and 'quit' in input_cmd:
        print_line()
        print_line('Bye!')
        print_line()
        return False
    return True

def print_scores(leaderboards):
    if any(leaderboards):
        print_line()
        print_line(' LEADERS AND BEST '.center(SW,'='))

    for x, leaderboard in enumerate(reversed(leaderboards)):
        y = len(leaderboards)-x-1
        if leaderboard:
            print_line()
            line = ' ' + DICT_NAMES[y] + 's '
            print_line(line.center(SW, '-'))
            print_line('#\tNam\tSco\tDate    \tWord'.expandtabs(5))
            for i, score_dict in enumerate(leaderboard):
                if not(i > 0 and score_dict['guesses'] == leaderboard[i-1]['guesses']):
                    rank = i+1
                try:
                    print_line('{}\t{}\t{}\t{}\t{}'.format(rank,score_dict['initials'],score_dict['guesses'],score_dict['date'],score_dict['word']).expandtabs(5))
                except:
                    print_line('{}\t{}\t{}\t{}\t{}'.format(rank,score_dict['initials'],score_dict['guesses'],score_dict['date'],'N/A').expandtabs(5))
        else:
            print_line()
            print_line('The {} Leaderboard is currently empty.'.format(DICT_NAMES[y]))

    if any(leaderboards):
        print_line()
        print_line(''.center(SW,'='))
        print_line()

def scoring(guesses=0, difficulty=0, random_word='', add=True):
    lb_file = PATH+'leaderboards.dat'

    if path.isfile(lb_file):
        if WEB and add:
            i = 0
            # wait for lock to be released
            while True:
                try:
                    f = open(lb_file, 'rb+')
                    fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
                    leaderboards = pickle.load(f)
                    f.seek(0)

                    break
                except IOError as e:
                    # raise on unrelated IOErrors
                    if e.errno != errno.EAGAIN:
                        print_line('File error!')
                        raise
                    else:
                        if i == 0:
                            print_line()
                        if i < 60:
                            if i % 10 == 0:
                                print_line('Waiting for Leaderboard access... ({} sec)'.format((i+1)*10))
                            sleep(1)
                            i += 1
                        else:
                            print_line('Leaderboard unavailable. Contact support.')
                            break
                else:
                    print_line('Unknown error with Leaderboard. Contact support.')
                    break
        else:
            with open(lb_file, 'rb') as f:
                leaderboards = pickle.load(f)
    else:
        if WEB and add:
            f = open(lb_file, 'wb')
        leaderboards = [[],[]]

    leaderboard = leaderboards[difficulty]
    board_size = 5
    if add:
        if len(leaderboard) < board_size or guesses < leaderboard[-1]['guesses']:
            print_line()
            if not leaderboard or guesses < leaderboard[0]['guesses']:
                print_line("Congratulations, you have the new top score!")
            else:
                print_line("Great job, you've made it to the Leaderboards!")
            initials = get_input("What are your initials?")[0:3].upper()
            score_dict = {'initials':initials, 'guesses':guesses, 'date':strftime('%x'), 'word':random_word[0:17]}
            if len(leaderboard) >= board_size:
                del leaderboard[-1]
            leaderboard.append(score_dict)
            leaderboard.sort(key=itemgetter('guesses'))

        if WEB:
            fcntl.flock(f, fcntl.LOCK_UN)
            pickle.dump(leaderboards, f)
            f.close()
        else:
            with open(lb_file, 'wb') as f:
                pickle.dump(leaderboards, f)

    print_scores(leaderboards)

def win(random_word, guesses, difficulty):
    print_line()
    print_line('You got it!')
    print_line('You guessed my word {} correctly after {} guesses.'.format(random_word, guesses))

    scaled_guesses = guesses - 5*difficulty;
    if scaled_guesses <= 10:
        print_line("Wowzers... nice job! How'd you get so good at this?")
    elif scaled_guesses <= 20:
        print_line("Wow, you're pretty good at this.")
    elif scaled_guesses <= 30:
        print_line("Not too bad!")
    elif scaled_guesses <= 40:
        print_line("Hey, not bad. Keep practicing.")
    else:
        print_line("Took you long enough.")

    scoring(guesses, difficulty, random_word)
    game_intro()

def game(word_lists, difficulty):
    print_line()
    print_line("All right, let's start.")
    print_line()
    print_line('To give up, say "give up".')
    print_line('To check the dictionary, say "take a peek".')
    print_line()
    print_line('Now let me think of a word...')
    print_line()
    
    word_list = word_lists[difficulty]
    master_word_list = word_lists[1]

    random_word = random.choice(word_list)
    low_word = word_list[0]
    high_word = word_list[-1]

    input_word = get_input("Okay, I'm thinking of a word between {} and {}.".format(low_word, high_word))

    guesses = 0
    while True:
        guesses += 1
        # Cheat Code
        #if 'xyzzy' in input_word:
        #    print_line('Dirty cheater...')
        #    print_line("(I'm thinking of {}.)".format(random_word))
        #    input_word = get_word(guesses, low_word, high_word)
        if 'give up' in input_word:
            guesses -= 1
            if 'y' in get_input('Sure you want to give up? (y/n)'):
                print_line('The word was {}. You gave up after {} guesses.'.format(random_word, guesses))
                game_intro()
                return
            else:
                print_line("Great, let's keep playing!")
                input_word = get_word(guesses, low_word, high_word)
        elif 'take a peek' in input_word:
            try:
                low_idx = word_list.index(low_word)
            except ValueError:
                for word in word_list:
                    if word > low_word:
                        low_idx = word_list.index(word)
                        break
            try:
                high_idx = word_list.index(high_word)
            except ValueError:
                for word in reversed(word_list):
                    if word < high_word:
                        high_idx = word_list.index(word)
                        break
            if high_idx - low_idx > 11:
                print_line('There are more than ten words left in range, so no peeking yet.')
            else:
                print_line()
                for word in word_list[low_idx:high_idx+1]:
                    print_line(word)
                print_line()
            input_word = get_word(guesses, low_word, high_word)
        elif input_word in master_word_list:
            if input_word >= high_word or input_word <= low_word:
                print_line("That's not in the range.")
                input_word = get_word(guesses, low_word, high_word)
            elif input_word > random_word:
                high_word = input_word
                input_word = get_word(guesses, low_word, high_word)
            elif input_word < random_word:
                low_word = input_word
                input_word = get_word(guesses, low_word, high_word)
            else:
                win(random_word, guesses, difficulty)
                return
        else:
            print_line("That's not in my dictionary.")
            input_word = get_word(guesses, low_word, high_word)

def main():
    game_intro(title=True)

    while parse_cmd(get_input()):
        continue

if __name__ == '__main__':
    main()
